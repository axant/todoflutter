import 'dart:async';
import 'package:flutter/material.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:flutter/widgets.dart';
import 'globals.dart' as globals;

class TodoEntry {
  final int id;
  final String text;
  final DateTime check;
  //final bool done;

  // TODO: set done boolean in constructor
  TodoEntry({this.id, this.text, this.check});

  TodoEntry.fromMap(Map<String, dynamic> map)
      : id = map['entry_id'],
        text = map['entry_text'],
        check = map['entry_check'] {
    print('TodoEntry.fromMap called id: $id');
  }

  // Convert a Dog into a Map. The keys must correspond to the names of the
  // columns in the database.
  Map<String, dynamic> toMap() {
    return {
      'entry_id': id,
      'entry_text': text,
      'entry_check': check,
    };
  }
}

void main() async {
  // Avoid errors caused by flutter upgrade.
  // Importing 'package:flutter/widgets.dart' is required.
  WidgetsFlutterBinding.ensureInitialized();
  final database = openDatabase(
    // Set the path to the database. Note: Using the `join` function from the
    // `path` package is best practice to ensure the path is correctly
    // constructed for each platform.
    join(await getDatabasesPath(), 'todo_entries.db'),
    // When the database is first created, create a table to store entries.
    onCreate: (db, version) {
      return db.execute(
        "CREATE TABLE entries (entry_id INTEGER PRIMARY KEY, entry_text TEXT, entry_check TEXT)",
      );
    },
    // Set the version. This executes the onCreate function and provides a
    // path to perform database upgrades and downgrades.
    version: 1,
  );
  
  Future<List<TodoEntry>> todoEntries() async {
    final Database db = await database;
    // Query the table for all The Entries.
    final List<Map<String, dynamic>> maps = await db.query('entries');
    // Convert the List<Map<String, dynamic> into a List<TodoEntry>.
    return List.generate(maps.length, (i) => TodoEntry.fromMap(maps[i]));
  }

  Future<void> insertEntry(TodoEntry entry) async {
    final Database db = await database;
    await db.insert(
      'entries',
      entry.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    print('wow');
    print(await todoEntries());
  }
  globals.insertEntry = insertEntry;
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'TodoFlutter',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'TodoFlutter'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<String> _entries = new List();

  void _addEntry(text) {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _entries.add(text);
      globals.insertEntry(TodoEntry(id: null, text: text, check: null));
    });
  }

  Function _deleteEntry(e) {
    return () {
      print('pressed: $e');
      setState(() {
          _entries.remove(e);
      });
      print('entries: $_entries');
    };
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          // Column is also a layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug painting" (press "p" in the console, choose the
          // "Toggle Debug Paint" action from the Flutter Inspector in Android
          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
          // to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          //mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Expanded(child: ListView(
              padding: const EdgeInsets.all(8),
              children: _entries.map<Widget>((e) => Container(
                  height: 30,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(e),
                      Container(
                        child: Row(
                          children: [
                            Text(e),
                            IconButton(
                              icon: Icon(Icons.delete),
                              tooltip: 'delete',
                              onPressed: _deleteEntry(e),
                            )
                          ]
                        )
                      )
                    ],
                  ),
              )).toList(),
            )),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => MyAddPage(title: 'Add Entry', addEntry: _addEntry)),
          );
        },
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

class MyAddPage extends StatefulWidget {
  MyAddPage({Key key, this.title, this.addEntry}) : super(key: key);

  final String title;
  final Function addEntry;

  @override
  _MyAddPageState createState() => _MyAddPageState();
}

class _MyAddPageState extends State<MyAddPage> {
  String addingText = '';
  TextEditingController addingTextController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        children: [
          TextField(
            maxLines: 8,
            controller: addingTextController,
            decoration: InputDecoration.collapsed(hintText: "Entry text here"),
          ),
          FlatButton(
            child: Text('+ ADD'),
            color: Colors.blue,
            textColor: Colors.white,
            onPressed: () {
              print('pressed add button');
              widget.addEntry(addingTextController.text);
              Navigator.pop(context);
            }
          )
        ]
      )
    );
  }
}
